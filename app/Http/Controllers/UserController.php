<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function store(Request $request){
        $usuario = new User();
        
        $usuario->name  = $request->nombre;
        $usuario->email = $request->email;
        $usuario->sexo  = $request->sexo;
        
        if($usuario->save()){
            return $this->responseMessage("success",200,"Guardo con exito");
        }else{
            return $this->responseMessage("error",500,"Ocurrio un error al guardar");
        }

    }
    
}
