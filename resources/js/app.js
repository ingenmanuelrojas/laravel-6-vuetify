import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'



Vue.use(VueRouter)
Vue.use(Vuetify);

import App from './views/App'
import Usuario from './views/Usuario'
import Home from './views/Home'
import Venta from './views/Venta'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/usuario',
            name: 'usuario',
            component: Usuario,
        },
        {
            path: '/ventas',
            name: 'ventas',
            component: Venta,
        },
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    vuetify: new Vuetify(),
    router,
});